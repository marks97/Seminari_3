/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>

int fibRecA(int n, int *asignaciones)
{
    if(n<=1)
    {
        return 1;
    }
    else
    {   (*asignaciones)++;
        return fibRecA(n-1, asignaciones) + fibRecA(n-2, asignaciones);
    }
}

int fibPDA(int n, int *asignaciones)
{
    
    int fibNumbers[n+1];
    int i;
    fibNumbers[0] = 1;
    fibNumbers[1] = 1;
    
    for(i=2;i<=n;i++)
    {
        fibNumbers[i] = fibNumbers[i-1] + fibNumbers[i-2];
        (*asignaciones)++;
    }
    return fibNumbers[n];
    
}

int main_ASS()
{
    int n;
    int asignaciones = 0;
    printf("Enter the index of the desired fibonacci number. \n");
    scanf("%d",&n);
    
    int fibon = fibPDA(n, &asignaciones);
    printf("Asignaciones PD -> %d\n",asignaciones);
    
    asignaciones = 0;
    int fibon2 = fibRecA(n,  &asignaciones);
    
    printf("Asignaciones REC -> %d\n",asignaciones);
    
    printf("The fibonacci number is %d\n", fibon);
    printf("The fibonacci number is %d\n", fibon2);
}