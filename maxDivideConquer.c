/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>

int MAX(int max1, int max2)
{
    if(max1 > max2)
    {
        return max1;
    }
    else
    {
        return max2;
    }
}

int max_recursive(int array[], int from, int to)
{
    int k;
    int MAX1,MAX2;
    
    if(from == to)
    {
        return array[from];
    }
    else
    {
        k = (from+to) / 2;
        printf("FROM %d, TO %d, K %d \n",from, to, k);

        MAX1 = max_recursive(array, from, k);
        MAX2 = max_recursive(array, k+1, to);
        return MAX(MAX1,MAX2);
    }

}

int main_max()
{
    int n,i;
    
    printf("Enter number of elements\n");
    scanf("%d", &n);
    printf("Enter %d integers\n", n);
    int array[n];
    
    for(i=0;i<n;i++)
    {
        scanf("%d",&array[i]);
    }
    
    int max = max_recursive(array, 0, n-1);
  
    printf("MAX IS %d \n",max);
  
}