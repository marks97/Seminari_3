/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdlib.h>

void combinatory_din(int F, int C, int coef[F][C]) {

    int i, j;

    for(i = 0; i < C; i++){

        for(j = 0; j < F; j++){

            if(j == 0 || i == j)
                coef[j][i] = 1;
            if(j == 1)
                coef[j][i] = i;
            else
                coef[j][i]=coef[j-1][i-1]+coef[j][i-1];
        }
    }
}

int main() {
    //int n,k;
    int F, C;
    int i, j;

    printf("Introduce the upper part \n");
    scanf("%d",&C);
    C++;
    printf("Introduce the lower part \n");
    scanf("%d",&F);
    F++;
    printf("Will compute this \n(%d)\n(%d)\n",C-1,F-1);

    //int comb = combinatory(n,k);
    int comb_din[F][C];
    combinatory_din(F, C, comb_din);


    printf("Result is:\n");

    for(j = 0; j < F; j++){

        printf("\n");

        for(i = 0; i < C; i++){
            printf(" %d  ", comb_din[j][i]);
        }
    }
}
