/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>

int fibPD(int n)
{
    
    int fibNumbers[n+1];
    int i;
    fibNumbers[0] = 1;
    fibNumbers[1] = 1;
    
    for(i=2;i<=n;i++)
    {
        fibNumbers[i] = fibNumbers[i-1] + fibNumbers[i-2];
    }
    return fibNumbers[n];
    
}

int fibPD_optimized(int n)
{
    if(n<=1)
    {
        return 1;
    }
    else
    {
        int prev1 = 1;
        int prev2 = 1;
        int actual = 0;
        int i=2;
        while(i<=n)
        {
            actual = prev1 + prev2;
            prev1 = prev2;
            prev2 = actual;
            i++;
        }
        return prev2;
    }
}


int main_PD()
{
    int n;
    printf("Enter the index of the desired fibonacci number. \n");
    scanf("%d",&n);
    
    int fibon = fibPD(n);
    
//    int fibon = fibPD_optimized(n);
    printf("The fibonacci number is %d\n", fibon);
}