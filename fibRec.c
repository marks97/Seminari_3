/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdlib.h>

int fibRec(int n)
{
    if(n<=1)
    {
        return 1;
    }
    else
    {
        return fibRec(n-1) + fibRec(n-2);
    }
}

int main_fibREC()
{
    int n;
    printf("Enter the index of the desired fibonacci number. \n");
    scanf("%d",&n);
    
    int fibon = fibRec(n);
    printf("The fibonacci number is %d\n", fibon);
}